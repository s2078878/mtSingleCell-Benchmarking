# mtSingleCell-Benchmarking - Single-Cell RNA-Sequencing mtDNA Variant Calling

This Nextflow pipeline will run all benchmarking conducted for the single-cell RNA-sequencing variant calling pipeline. 

**NOTE: Because variant calling benchmarking data is currently unavailable for 10x single-cell RNA-sequencing in a lineage tracing context, benchmarking was conducted using mitochondrial mutational signatures as validation. To fully replicate our benchmarking, you will require access to the single-cell sample used, although this pipeline will still run equally as well on any 10x single-cell RNA-sequencing data available. To run the pipeline, use the instrucitons below.**

## Running the Pipeline

### 1. Cloning Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/mtSingleCell-Benchmarking.git
cd mtSingleCell-Benchmarking
```

### 2. Create Environment

To create and activate a conda environment with all the necessary packages, run the following command:

```shell
conda env create --file env.yml
conda activate mtSingleCell-Benchmarking
```

### 3. Make Support Files

#### Matched-Samples
The Nextflow pipeline works from a .csv file containing the main parameters needed. The csv file should have the following format and column names:

| row | sampleID | bam                           | index                         |
|:----|:---------|:------------------------------|:------------------------------|
| 1   | sample1  | path/to/sample1/alignment.bam | path/to/sample1/index.bam.bai |
| 2   | sample2  | path/to/sample2/alignment.bam | path/to/sample2/index.bam.bai |
| 3   | sample3  | path/to/sample3/alignment.bam | path/to/sample3/index.bam.bai |
| 4   | sample4  | path/to/sample4/alignment.bam | path/to/sample4/index.bam.bai |

Alignment and index files can be for the entire genome, there is no need to subset to only the mitochondrial DNA.

### 4. Run mtSingleCell-Benchmarking

Everything should now be ready to run mtSingleCell-Benchmarking. A typical command will look something similar to the one below, substituting in the path to your own parameter file. Take additional note of setting the `--chr` parameter determining how the mitochondrial genome is denoted in the alignment files. 

```shell
nextflow run mtSingleCell-Benchmarking.nf \
  --parameters path/to/parameters/file.csv \ # parameters file (see step 3)
  --outdir path/to/out/directory/ \ # where should the results folder be created? default: run directory
  --chr (chrM / MT) # how is the mitochondrial genome denoted? default: chrM
```

### 5. Outputs

The output .pdf file('s) will be generated in the results directory. One .pdf file will be generated for each sample input, this will contain benchmarking results for each stage conducted that led us to the final developed pipeline. 

## Notes

Please be aware that this pipelines `nextflow.config` file has been configured to run on the University of Edinburgh's Eddie HPC Cluster. This may need to be reconfigured to run on other computing systems. 







