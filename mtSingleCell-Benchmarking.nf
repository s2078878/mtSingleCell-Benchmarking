#!/usr/bin/env nextflow

log.info """\
         ===========================================================================================================
         M T S I N G L E C E L L - B E N C H M A R K I N G :   S I N G L E   C E L L   V A R I A N T   C A L L I N G
         ===========================================================================================================
         parameters_file:   ${params.parameters}
         ===========================================================================================================
         """
         .stripIndent()


// make pipeline output one html document, with each page showing benchmark results for each sample?

/*
 * Input parameters validation
 */
if (params.parameters){
    
    parameters_file = file(params.parameters)

    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.bam, row.index, row.barcodes)}
        .set{ parameters_ch }
    
    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

/*
 * Extract only the mitochondria reads from the bam files
 * Filter to only reads with mapping quality greater than 30 and remove supplementary and secondary alignments
 */
process chrm {
    tag "Extracting mitochondrial reads"

    input:
    set row, sampleID, path(bam), path(index), path(barcodes) from parameters_ch

    output:
    set sampleID, path("${sampleID}.MT.bam"), path("${sampleID}.MT.bam.bai"), path(barcodes) into mitochondria_ch, mitochondria_ch2, mitochondria_ch3, mitochondria_ch4

    script:
    """
    samtools view -h -b -q 30 -F 2304 ${bam} MT > ${sampleID}.MT.bam
    samtools index ${sampleID}.MT.bam
    """
}

/*
 * Split bam file into cells
 */
process split {
    tag "Splitting bam into cells"

    input:
    set sampleID, path(bam), path(index), path(barcodes) from mitochondria_ch

    output:
    set sampleID, path("cell*.bam"), path(barcodes) into cells_ch, cells_ch2, cells_ch3, cells_ch4

    script:
    """
    cp $baseDir/scripts/split.py . 
    python split.py ${bam} ${barcodes}
    """
}

/*
 * Call all mitochondrial variants for each cell using the custom variant calling script
 */
process custom_cell {
    tag "Running Custom Caller on Individual Cells"

    input:
    set sampleID, path(cell_bams), path(barcodes) from cells_ch

    output:
    set sampleID, path("cell*.custom.txt"), path(barcodes) into custom_cell_ch

    script:
    """
    ls *.bam | parallel 'samtools index {}'
    cp $baseDir/scripts/call.py $baseDir/data/MT* .
    python call.py ${cell_bams}
    """
}

/*
 * Call all mitochondrial variants for each cell using varscan2
 */
process varscan_cell {
    tag "Running VarScan2 on Individual Cells"

    input:
    set sampleID, path(cell_bams), path(barcodes) from cells_ch2

    output:
    set sampleID, path("cell*.varscan.txt"), path(barcodes) into varscan_cell_ch

    script:
    """
    ls *.bam | parallel 'samtools index {}'

    cp $baseDir/data/MT.fa .

    for cell in ${cell_bams}
    do

    name="\$(basename -s .bam "\$cell")"

    samtools sort \${cell} | \
        samtools mpileup -d 0 -f MT.fa - | \
        varscan mpileup2snp --strand-filter 1 --min-coverage 1 --min-reads2 1 --min-avg-qual 30 --min-var-freq 0 > "\${name}.varscan.txt"

    done
    """
}

/*
 * Call all mitochondrial variants for each cell using bcftools
 */
process bcftools_cell {
    tag "Running BCFTools on Individual Cells"

    input:
    set sampleID, path(cell_bams), path(barcodes) from cells_ch3

    output:
    set sampleID, path("cell*.bcftools.vcf"), path(barcodes) into bcftools_cell_ch

    script:
    """
    ls *.bam | parallel 'samtools index {}'

    cp $baseDir/data/MT.fa .

    for cell in ${cell_bams}
    do

    name="\$(basename -s .bam "\$cell")"
    
    bcftools mpileup -I -Q 30 -A -x -Ou -d 1000000000 -f MT.fa \${cell} | \
        bcftools call -Ov -mv > "\${name}.bcftools.vcf"
    
    done
    """
}

/*
 * Call all mitochondrial variants for each cell using freebayes
 */
process freebayes_cell {
    tag "Running Freebayes on Individual Cells"

    input:
    set sampleID, path(cell_bams), path(barcodes) from cells_ch4

    output:
    set sampleID, path("cell*.freebayes.vcf"), path(barcodes) into freebayes_cell_ch

    script:
    """
    ls *.bam | parallel 'samtools index {}'

    cp $baseDir/data/MT.* .

    for cell in ${cell_bams}
    do

    name="\$(basename -s .bam "\$cell")"

    freebayes -f MT.fa --min-base-quality 30 \${cell} > "\${name}.freebayes.vcf"
    
    done
    """
}

custom_cell_ch
    .mix(varscan_cell_ch)
    .mix(bcftools_cell_ch)
    .mix(freebayes_cell_ch)
    .set { merged_ch }

/*
 * Merge all single cell variant calls
 */
process merge_cells {
    tag "Merging individual cell variant calls"
    publishDir "${baseDir}/results/" , mode:'copy'

    input:
    set sampleID, path(calls), path(barcodes) from merged_ch

    output:
    path("${sampleID}_*_cells.txt") into results_ch

    script:
    """
    cp $baseDir/scripts/merge.R .
    Rscript merge.R ${sampleID} ${barcodes} ${calls} 
    """
}

/*
 * Call all mitochondrial variants for pseudobulk using varscan2
 */
process varscan_pseudobulk {
    tag "Running VarScan2 on Pseudobulk"
    publishDir "${baseDir}/results/" , mode:'copy'

    input:
    set sampleID, path(bam), path(index), path(barcodes) from mitochondria_ch2

    output:
    set sampleID, path("${sampleID}.varscan.txt"), path(barcodes) into varscan_pseudo_ch

    script:
    """
    cp $baseDir/data/MT.fa .
    samtools sort ${bam} | \
        samtools mpileup -d 0 -f MT.fa - | \
        varscan mpileup2snp --strand-filter 1 --min-coverage 1 --min-reads2 1 --min-avg-qual 30 --min-var-freq 0 > ${sampleID}.varscan.txt
    """
}

/*
 * Call all mitochondrial variants for pseudobulk using bcftools
 */
process bcftools_pseudobulk {
    tag "Running BCFTools on Pseudobulk"
    publishDir "${baseDir}/results/" , mode:'copy'

    input:
    set sampleID, path(bam), path(index), path(barcodes) from mitochondria_ch3

    output:
    set sampleID, path("${sampleID}.bcftools.vcf"), path(barcodes) into bcftools_pseudo_ch

    script:
    """
    cp $baseDir/data/MT.fa .
    bcftools mpileup -I -Q 30 -A -x -Ou -d 1000000000 -f MT.fa ${bam} | \
        bcftools call -Ov -mv > ${sampleID}.bcftools.vcf
    """
}

/*
 * Call all mitochondrial variants for pseudobulk using freebayes
 */
process freebayes_pseudobulk {
    tag "Running Freebayes on Pseudobulk"
    publishDir "${baseDir}/results/" , mode:'copy'

    input:
    set sampleID, path(bam), path(index), path(barcodes) from mitochondria_ch4

    output:
    set sampleID, path("${sampleID}.freebayes.vcf"), path(barcodes) into freebayes_pseudo_ch

    script:
    """
    cp $baseDir/data/MT.* .
    freebayes -f MT.fa --min-base-quality 30 ${bam} > ${sampleID}.freebayes.vcf
    """
}

// /*
//  * Call all mitochondrial variants for pseudobulk using freebayes
//  */
// process benchmark {
//     tag "Running benchmarking"

//     input:

//     output:

//     script:
//     """
//     """
// }

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All mitochondrial DNA variants called for single-cells.\n" : "\nOops .. something went wrong.\n" )
}
