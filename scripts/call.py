import pysam
import sys
import os

## Read in reference file so is ready to index for each position
with open("MT.fa", "r") as fh:
    line = fh.readline()
    header = ""
    ref = ""
    while line:
        line = line.rstrip('\n')
        if ">" in line:
            header = line
        else:
            ref = ref + line
        line = fh.readline()


for i in sys.argv[1:]:
    ## Read in bam file to have variants calle
    samfile = pysam.AlignmentFile(i, "rb")

    base_name = os.path.splitext(os.path.basename(i))[0]

    ## open output file for variants
    out = open(base_name + ".custom.txt",'w')
    out.write("chr\tpos\talt\tref\tvaf\tsupporting\ttotal\tmean.bq\tr1+\tr1-\tr2+\tr2-\n")

    ## Set count values to 0 for number of supporting reads and total base qualities
    A_count = 0
    C_count = 0
    G_count = 0 
    T_count = 0

    A_total_bq = 0
    C_total_bq = 0
    G_total_bq = 0 
    T_total_bq = 0

    r1_plus = 0
    r1_minus = 0

    A_r2_plus = 0
    A_r2_minus = 0
    
    C_r2_plus = 0
    C_r2_minus = 0
    
    G_r2_plus = 0
    G_r2_minus = 0
    
    T_r2_plus = 0
    T_r2_minus = 0

    ## Loop through all of the bases in chrM
    for pileupcolumn in samfile.pileup(region="MT", max_depth=10000000, min_base_quality=0):
        
        #line1 = str("coverage at base ") + str(pileupcolumn.pos) + str(" = ") + str(pileupcolumn.n) + "\n"
        
        # Now loop through each read covering that individual base for this loop
        for pileupread in pileupcolumn.pileups:
            
            if not pileupread.is_del and not pileupread.is_refskip:

                ref_base = str(ref[pileupcolumn.pos])
                base = str(pileupread.alignment.query_sequence[pileupread.query_position])
                quality = pileupread.alignment.query_qualities[pileupread.query_position]
                is_rev = pileupread.alignment.is_reverse

                # count the number of reads supporting each base and add up base qualities ready to calculate mean
                if base == "A":

                    A_count = A_count + 1
                    A_total_bq = A_total_bq + quality

                    if is_rev and base != ref_base:
                        A_r2_minus = A_r2_minus + 1
                    elif not is_rev and base != ref_base:
                        A_r2_plus = A_r2_plus + 1
                    else:
                        pass

                elif base == "C":
                    
                    C_count = C_count + 1
                    C_total_bq = C_total_bq + quality

                    if is_rev and base != ref_base:
                        C_r2_minus = C_r2_minus + 1
                    elif not is_rev and base != ref_base:
                        C_r2_plus = C_r2_plus + 1
                    else:
                        pass

                elif base == "G":
                    
                    G_count = G_count + 1
                    G_total_bq = G_total_bq + quality

                    if is_rev and base != ref_base:
                        G_r2_minus = G_r2_minus + 1
                    elif not is_rev and base != ref_base:
                        G_r2_plus = G_r2_plus + 1
                    else:
                        pass

                elif base == "T":
                    
                    T_count = T_count + 1
                    T_total_bq = T_total_bq + quality

                    if is_rev and base != ref_base:
                        T_r2_minus = T_r2_minus + 1
                    elif not is_rev and base != ref_base:
                        T_r2_plus = T_r2_plus + 1
                    else:
                        pass
                
                else:
                    pass


                if base == ref_base:
                    
                    if is_rev:
                        r1_minus = r1_minus + 1
                    elif not is_rev:
                        r1_plus = r1_plus + 1
                    else:
                        pass
                
                else:
                    pass


        # calculate the mean base quality and allele frequency
        A_mean = ( A_total_bq / A_count ) if A_count != 0 else 0
        C_mean = ( C_total_bq / C_count ) if C_count != 0 else 0
        G_mean = ( G_total_bq / G_count ) if G_count != 0 else 0
        T_mean = ( T_total_bq / T_count ) if T_count != 0 else 0

        A_vaf = round((A_count / pileupcolumn.n) * 100, 3)
        C_vaf = round((C_count / pileupcolumn.n) * 100, 3)
        G_vaf = round((G_count / pileupcolumn.n) * 100, 3)
        T_vaf = round((T_count / pileupcolumn.n) * 100, 3)

        # if there is supporting reads and they're not the same as the reference, output the variant to file
        if A_count != 0 and ref_base != "A":
            A_allele = "chrM" + "\t" + str(pileupcolumn.pos + 1) + "\t" + "A" + "\t" + ref_base + "\t" + str(A_vaf) + "\t" + str(A_count) + "\t" + str(pileupcolumn.n) + "\t" + str(round(A_mean, 3)) + "\t" + str(r1_plus) + "\t" + str(r1_minus) + "\t" + str(A_r2_plus) + "\t" + str(A_r2_minus) + "\n"
            out.write(A_allele)
        
        if C_count != 0 and ref_base != "C":
            C_allele = "chrM" + "\t" + str(pileupcolumn.pos + 1) + "\t" + "C" + "\t" + ref_base + "\t" + str(C_vaf) + "\t" + str(C_count) + "\t" + str(pileupcolumn.n) + "\t" + str(round(C_mean, 3)) + "\t" + str(r1_plus) + "\t" + str(r1_minus) + "\t" + str(C_r2_plus) + "\t" + str(C_r2_minus) + "\n"
            out.write(C_allele)

        if G_count != 0 and ref_base != "G":
            G_allele = "chrM" + "\t" + str(pileupcolumn.pos + 1) + "\t" + "G" + "\t" + ref_base + "\t" + str(G_vaf) + "\t" + str(G_count) + "\t" + str(pileupcolumn.n) + "\t" + str(round(G_mean, 3)) + "\t" + str(r1_plus) + "\t" + str(r1_minus) + "\t" + str(G_r2_plus) + "\t" + str(G_r2_minus) + "\n"
            out.write(G_allele)

        if T_count != 0 and ref_base != "T":
            T_allele = "chrM" + "\t" + str(pileupcolumn.pos + 1) + "\t" + "T" + "\t" + ref_base + "\t" + str(T_vaf) + "\t" + str(T_count) + "\t" + str(pileupcolumn.n) + "\t" + str(round(T_mean, 3)) + "\t" + str(r1_plus) + "\t" + str(r1_minus) + "\t" + str(T_r2_plus) + "\t" + str(T_r2_minus) + "\n"
            out.write(T_allele)

        # reset counts ready for the next base position
        A_count = 0
        C_count = 0
        G_count = 0 
        T_count = 0

        A_total_bq = 0
        C_total_bq = 0
        G_total_bq = 0 
        T_total_bq = 0

        r1_plus = 0
        r1_minus = 0

        A_r2_plus = 0
        A_r2_minus = 0

        C_r2_plus = 0
        C_r2_minus = 0

        G_r2_plus = 0
        G_r2_minus = 0

        T_r2_plus = 0
        T_r2_minus = 0

# close the output file and bam file being tested
out.close()
samfile.close()